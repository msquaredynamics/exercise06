package com.msquaredynamics.exercise06.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.msquaredynamics.exercise06.data.models.Article
import com.msquaredynamics.exercise06.data.repositories.ArticleRepository


class ArticleViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var selectedArticleId: MutableLiveData<Int>
    private val repository: ArticleRepository = ArticleRepository(application)

    private lateinit var articles: LiveData<List<Article>>


    /**
     * @return articles The list of the articles, wrapped in a LiveData object.
     */
    fun getArticles(): LiveData<List<Article>> {
        if (!::articles.isInitialized) {
            articles = repository.getAllArticles()
        }

        return articles
    }



    /**
     * Return the ID of the selected article
     * @return selectedArticleId LiveData object containing the selected article ID
     */
    fun getSelectedArticleId(): LiveData<Int> {
        if (!::selectedArticleId.isInitialized) {
            selectedArticleId = MutableLiveData()
            selectedArticleId.value = -1
        }

        return selectedArticleId
    }


    /**
     * Set an article as "selected"
     * @param articleId ID of the article to select
     */
    fun selectArticle(articleId: Int) {
        if (!::selectedArticleId.isInitialized) {
            selectedArticleId = MutableLiveData()
        }
        selectedArticleId.value = articleId
    }


    /**
     * Deletes an article
     * @param articleId ID of the article to delete
     */
    fun deleteArticle(articleId: Int) {
        articles.value?.find {
            it.id == articleId
        }?.also {
            repository.deleteArticle(it)
        }
    }


    /**
     * Moves an article
     * @param from Original position
     * @param to   Final position
     */
    fun moveArticle(from: Int, to: Int) {
        if (from == to) {
            return
        }
        repository.moveArticle(from, to)
    }
}