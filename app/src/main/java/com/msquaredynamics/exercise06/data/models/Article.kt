package com.msquaredynamics.exercise06.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "articles")
data class Article(var title: String, var description: String, var position: Int, @PrimaryKey(autoGenerate = true) var id: Int = 0)