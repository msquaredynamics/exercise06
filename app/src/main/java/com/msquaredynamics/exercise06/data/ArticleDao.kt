package com.msquaredynamics.exercise06.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.msquaredynamics.exercise06.data.models.Article

@Dao
interface ArticleDao {
    @Query("SELECT * FROM articles ORDER BY position")
    fun getAll(): LiveData<List<Article>>

    @Query("SELECT * FROM articles WHERE id = :articleId")
    fun getById(articleId: Int): LiveData<Article>

    @Insert
    fun insert(article: Article)

    @Insert
    fun insertAll(articles: List<Article>)

    @Delete
    fun delete(article: Article)

    @Query("DELETE FROM articles")
    fun deleteAll()

    @Update
    fun updateAll(articles: List<Article>)
}