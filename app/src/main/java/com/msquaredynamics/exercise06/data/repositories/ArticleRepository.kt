package com.msquaredynamics.exercise06.data.repositories

import android.content.Context
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.msquaredynamics.exercise06.data.AppDatabase
import com.msquaredynamics.exercise06.data.ArticleDao
import com.msquaredynamics.exercise06.data.models.Article


class ArticleRepository(context: Context) {
    private val db: AppDatabase = AppDatabase.getInstance(context)
    private val articleDao = db.articleDao()

    private lateinit var articles: LiveData<List<Article>>



    /**
     * Returns all the articles from the database
     */
    fun getAllArticles(): LiveData<List<Article>> {
        if (!::articles.isInitialized) {
            articles = articleDao.getAll()
        }

        return articles
    }


    fun deleteArticle(article: Article) {
        DeleteAsyncTask(articleDao).execute(article)
    }


    /**
     * Moves an article from a position into another
     * @param from Old position
     * @param to   New position
     */
    fun moveArticle(from: Int, to: Int) {
        val data = articles.value!!

        /**
         * Note that this will modify the objects wrapped into the articles LiveData, without that the observers are
         * notified of that. In this case this is not an issue since we are updating the database, therefore the observers
         * will receive the updated value when they change on the database.
         */
        if (from < to) {
            for (i in (from + 1)..to) {
                data[i].position = data[i].position - 1
            }
        } else {
            for (i in (from-1) downTo to) {
                data[i].position = data[i].position + 1
            }
        }
        data[from].position = to

        UpdateAsyncTask(articleDao).execute(data)
    }
}



private class DeleteAsyncTask(val articleDao: ArticleDao) : AsyncTask<Article, Unit, Unit>() {
    override fun doInBackground(vararg params: Article?) {
        articleDao.delete(params[0]!!)
    }
}


private class UpdateAsyncTask(val articleDao: ArticleDao) : AsyncTask<List<Article>, Unit, Unit>() {
    override fun doInBackground(vararg params: List<Article>?) {
        articleDao.updateAll(params[0]!!)
    }
}