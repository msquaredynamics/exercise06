package com.msquaredynamics.exercise06.data

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.msquaredynamics.exercise06.R
import com.msquaredynamics.exercise06.data.models.Article


@Database(entities = arrayOf(Article::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun articleDao(): ArticleDao


    companion object {
        // A Kotlin bug prevents to use lateinit variables in companion objects. See https://youtrack.jetbrains.com/issue/KT-21862
        // for the official issue tracker.
        @JvmStatic
        private var singleton: AppDatabase? = null

        @JvmStatic
        private val dbCallback = object : RoomDatabase.Callback() {
            // Will be invoked after the database is initialized and ready. See Room documentation
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                PopulateDbAsync(singleton!!).execute()
            }
        }

        /**
         * Returns a singleton instnce of the AppDatabase class. The first time it is called, this method will
         * initialise the database. Subsequent calls will always return the same instance.
         */
        @JvmStatic
        fun getInstance(context: Context): AppDatabase {
            // Set to true for rebuilding the database every time the app starts
            val ALWAYS_REBUILD_DB = true

            if (singleton == null) {
                val prefs = context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE)

                singleton = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "app_database").apply {

                    // If this is the first time the app is run, we populate the database
                    if (prefs.getBoolean("firstRun", true)) {
                        prefs.edit().putBoolean("firstRun", false).apply()
                        addCallback(dbCallback)
                    } else if (ALWAYS_REBUILD_DB) {

                        // We populate the database also if the ALWAYS_REBUILD_DB is set to true
                        addCallback(dbCallback)
                    }
                }
                    .build()
            }

            return singleton!!
        }
    }
}


/**
 * AsyncTask that deletes all data in the database and then inserts new data.
 */
private class PopulateDbAsync(private val db: AppDatabase) : AsyncTask<Unit, Unit, Unit>() {
    private val articleDao = db.articleDao()

    override fun doInBackground(vararg params: Unit?) {
        articleDao.deleteAll()

        List(20) {
            Article("This is art. $it", "This is the description of the article with ID = $it", it)
        }.also {
            articleDao.insertAll(it)
        }
    }
}

