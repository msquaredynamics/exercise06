package com.msquaredynamics.exercise06

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.msquaredynamics.exercise06.viewmodels.ArticleViewModel
import kotlinx.android.synthetic.main.fragment_details.view.*


class DetailsFragment : androidx.fragment.app.Fragment() {
    private lateinit var articleViewModel: ArticleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.run {
            articleViewModel = ViewModelProviders.of(this).get(ArticleViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_details, container, false)

        // When we register the Observer, we'll immediately receive the last value
        articleViewModel.getSelectedArticleId().observe(this, Observer {
            if (it != -1) {
                val article = articleViewModel.getArticles().value?.find { item ->
                    item.id == it
                }

                mView.textview_frag_details_title.text = article?.title
                mView.textview_frag_details_description.text = article?.description
            }
        })

        return mView
    }
}
