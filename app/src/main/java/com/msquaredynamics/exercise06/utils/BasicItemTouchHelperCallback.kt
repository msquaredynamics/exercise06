package com.msquaredynamics.exercise06.utils

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper

/**
 * This class implements the ItemTouchHelper callback interface that is used by RecyclerView classes to handle drag and
 * swipe gestures.
 */
class BasicItemTouchHelperCallback(
    val adapter: ItemTouchHelperAdapter,
    private val longPressDragEnabled: Boolean = false,
    private val swipeEnabled: Boolean = false): ItemTouchHelper.Callback()
{

    // Variables to track the position of the dragged element inside the list
    private var dragFrom = -1
    private var dragTo = -1

    override fun getMovementFlags(p0: RecyclerView, p1: RecyclerView.ViewHolder): Int {
        val dragFlags = ItemTouchHelper.UP or ItemTouchHelper.DOWN

        val swipeFlags = if (adapter.isSwipeable(p1.adapterPosition)) ItemTouchHelper.END else 0
        return makeMovementFlags(dragFlags, swipeFlags)
    }


    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, targetViewHolder: RecyclerView.ViewHolder): Boolean {
        val fromPosition = viewHolder.adapterPosition
        val toPosition = targetViewHolder.adapterPosition

        if (dragFrom == -1) {
            dragFrom = fromPosition
        }

        dragTo = toPosition

        adapter.onItemMove(fromPosition, toPosition)
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        adapter.onItemDismiss(viewHolder.adapterPosition)
    }

    override fun isLongPressDragEnabled() = longPressDragEnabled
    override fun isItemViewSwipeEnabled() = swipeEnabled


    // Called when a Drag or Swipe operation terminates. Check the values of dragFrom and dragTo to check if the
    // operation was a drag/drop and only notifies
    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)

        if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo) {
            adapter.onItemMoveFinished(dragFrom, dragTo)
        }

        dragFrom = -1
        dragTo = -1
    }

    /**
     * Interface that must be implemented by the RecyclerView's adapter, used to pass the events up to the chain.
     */
    interface ItemTouchHelperAdapter {
        /** Notify the adapter that an item has been moved, but dragging is still active. Called every time the item
         * is moved above or below another item
         *
         * @param from Position of the element
         * @param to   Position of the element over which this element has been dragged
         */
        fun onItemMove(from: Int, to: Int)

        /** Notify the adapter that the drag operation has finished
         * @param from Original position of the moved element
         * @param to Final position of the moved element
         */
        fun onItemMoveFinished(from: Int, to: Int)

        /** Notify the adapter that an item has been swiped out and dismissed */
        fun onItemDismiss(position: Int)

        /** Checks if the element at a given position can be swiped
         * @param position Element position in the adapter
         * @return True if the element can be swiped
         */
        fun isSwipeable(position: Int) : Boolean
    }
}