package com.msquaredynamics.exercise06.utils

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View


/**
 * Enhanced RecyclerView that provides single tap and long click capabilities on its items out of the box.
 *
 * NOTE
 * This implementation has the advantage of decoupling the "click" listeners from the RecyclerView's Adapter class.
 * However, while it provides listeners for click events on single rows, it is unable to detect clicks or events on
 * items inside these rows. In other word, this is suitable if we are interested only in the "click" event on a certain
 * row of the list, without further knowledge of which part of the row was clicked.
 * If the latter information is required, this solution is not compatible and one should go with the single "onClickListener"
 * listeners inside the Adapter class itself.
 */
class SelectRecyclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet?, defStyle: Int = 0) : androidx.recyclerview.widget.RecyclerView(context, attrs, defStyle) {

    var itemClickListener : ItemClickListener? = null

    init {
        addOnItemTouchListener(
            RecyclerTouchListener(
                context,
                this,
                object : ItemClickListener {
                    override fun onClick(position: Int, view: View) {
                        itemClickListener?.onClick(position, view)
                    }

                    override fun onLongClick(position: Int, view: View) {
                        itemClickListener?.onLongClick(position, view)
                    }
                })
        )
    }


    /**
     * Interface used to notify about detected touch events on the RecyclerView items
     */
    interface ItemClickListener {
        /**
         * Called when a single tap is detected on any item
         * @param position The position in the adapter of clicked item
         * @param view     The view that was clicked
         */
        fun onClick(position: Int, view: View)


        /**
         * Called when a long tap is detected on any item
         * @param position The position in the adapter of long clicked item
         * @param view     The view that was clicked
         */
        fun onLongClick(position: Int, view: View)
    }



    /**
     * Inner class responsible for capturing click events on the RecyclerView. It extends the base SimpleOnTouchListener
     * class which provides stub implementations of the OnItemTouchListener interface. The RecyclerView receives click
     * events through the ItemClickListener interface.
     *
     * The class intercepts motion events on the ViewGroup through the onInterceptTouchEvent method, and process them
     * before they are dispatched to the RecyclerView's child views.
     * These motion events are processed using the GestureDetector instance by calling its onTouchEvent(MotionEvent). The
     * GestureDetector inspects the received MotionEvent and calls the appropriate callback method in its OnGestureListener.
     */
    private class RecyclerTouchListener(context: Context, val recyclerView: androidx.recyclerview.widget.RecyclerView, var clickListener: ItemClickListener?) : androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener() {
        private var gestureDetector: GestureDetector? = null

        init {
            /**
             * Initializes the GestureDetector. The SimpleOnGestureListener is a framework class that implements the
             * OnGestureListener interface with stub implementation of all its methods. Extending this class enables us
             * to override only the methods we are interested on (and not all the methods in the interface)
             */
            val gestureListener: GestureDetector.SimpleOnGestureListener = object : GestureDetector.SimpleOnGestureListener() {

                /** Called when a tap occurs with the up MotionEvent that triggered it. */
                override fun onSingleTapUp(e: MotionEvent?): Boolean {
                    return true // True means that the event was consumed by this listener
                }

                /** Called when a long press occurs with the initial on down MotionEvent that trigged it. */
                override fun onLongPress(e: MotionEvent?) {
                    e?.let {
                        recyclerView.findChildViewUnder(e.x, e.y)?.let {
                            clickListener?.onLongClick(recyclerView.getChildAdapterPosition(it), it)
                        }
                    }

                }
            }

            gestureDetector = GestureDetector(context, gestureListener)
        }


        /**
         * Intercepts all MotionEvent on the surface of the RecyclerView, and processes them first before sending them
         * to the RecyclerView's children.
         */
        override fun onInterceptTouchEvent(rv: androidx.recyclerview.widget.RecyclerView, e: MotionEvent): Boolean {

            rv.findChildViewUnder(e.x, e.y)?.let {

                if (clickListener != null && gestureDetector!!.onTouchEvent(e)) {
                    // gestureDetector.onTouchEvent() returns true if a single tap was detected
                    clickListener?.onClick(rv.getChildAdapterPosition(it), it)
                }
            }

            return false
        }
    }
}


